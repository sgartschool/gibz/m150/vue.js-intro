import Vue from 'vue'
import Vuex from 'vuex'
import joke from "@/store/modules/joke";

Vue.use(Vuex)

export interface GlobalState {
    loadingTargets: string[];
}

export default new Vuex.Store({
    state: {
        loadingTargets: [],
    } as GlobalState,

    getters: {
        anythingLoading: state => state.loadingTargets.length === 0,
        isLoading: state => (loadingTarget: string) => state.loadingTargets.includes(loadingTarget),
    },

    actions: {
        START_LOADING({commit}, loadingTarget: string) {
            commit('SET_LOADING_STATE', {loadingTarget, isLoading: true});
        },

        FINISH_LOADING({commit}, loadingTarget: string) {
            commit('SET_LOADING_STATE', {loadingTarget, isLoading: false});
        }
    },

    mutations: {
        SET_LOADING_STATE(state, {loadingTarget, isLoading}: { loadingTarget: string; isLoading: boolean }) {
            if (isLoading) {
                if (!state.loadingTargets.includes(loadingTarget)) {
                    state.loadingTargets.push(loadingTarget);
                }
            } else {
                if (state.loadingTargets.includes(loadingTarget)) {
                    let targetIndex = -1;
                    do {
                        targetIndex = state.loadingTargets.indexOf(loadingTarget, targetIndex + 1);
                        if (targetIndex >= 0) {
                            state.loadingTargets.splice(targetIndex, 1);

                        }
                    } while (targetIndex !== -1);
                }
            }
        }
    },

    modules: {
        joke,
    }

})
